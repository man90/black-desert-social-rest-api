# BDO-REST-API
This repository used to contain a scraper for Black Desert Online community data with a built-in REST API server.

## The source code is now stored on [GitHub](https://github.com/man90es/BDO-REST-API).
